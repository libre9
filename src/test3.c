#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "libre9/re9.h"


static const char text[] = "test\0for zero";


static void dump_matches (const char *str, const re9_sub_t *rs, int ms) {
  for (int c = 0; c < 10; ++c) {
    if (rs[c].sp != NULL && rs[c].ep != NULL) {
      fprintf(stdout, " %d:(%d,%d) <", c, rs[c].sp-str, rs[c].ep-str);
      fwrite(rs[c].sp, rs[c].ep-rs[c].sp, 1, stdout);
      fputc('>', stdout);
    }
  }
  fputc('\n', stdout);
}


int main (int argc, char *argv[]) {
  re9_sub_t rs[10];
  re9_prog_t *p0, *p1, *p2;
  const char *errmsg;
  /* */
  p0 = re9_compile("t\\zfor", RE9_FLAG_NONUTF8, &errmsg);
  if (p0 == NULL) { fprintf(stderr, "FATAL: invalid regexp: %s\n", errmsg); exit(2); }
  p1 = re9_compile("\\zfor", RE9_FLAG_NONUTF8, &errmsg);
  if (p1 == NULL) { fprintf(stderr, "FATAL: invalid regexp: %s\n", errmsg); exit(2); }
  p2 = re9_compile("[a\\z]for", RE9_FLAG_NONUTF8, &errmsg);
  if (p1 == NULL) { fprintf(stderr, "FATAL: invalid regexp: %s\n", errmsg); exit(2); }
  /* */
  printf("=============================== (%u)\n", sizeof(text));
  rs[0].sp = text;
  rs[0].ep = text+sizeof(text);
  if (re9_execute(p0, RE9_FLAG_NONUTF8|RE9_FLAG_MT0_RANGE, text, rs, 10) > 0) dump_matches(text, rs, 10);
  printf("=============================== (%u)\n", sizeof(text));
  rs[0].sp = text;
  rs[0].ep = text+sizeof(text);
  if (re9_execute(p1, RE9_FLAG_NONUTF8|RE9_FLAG_MT0_RANGE, text, rs, 10) > 0) dump_matches(text, rs, 10);
  printf("=============================== (%u)\n", sizeof(text));
  rs[0].sp = text;
  rs[0].ep = text+sizeof(text);
  if (re9_execute(p2, RE9_FLAG_NONUTF8|RE9_FLAG_MT0_RANGE, text, rs, 10) > 0) dump_matches(text, rs, 10);
  /* */
  re9_free(p2);
  re9_free(p1);
  re9_free(p0);
  /* */
  return 0;
}
