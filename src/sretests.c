#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libre9/re9.h"

#include "testregexps.c"


int main (int argc, char *argv[]) {
  int c;
  re9_sub_t subs[RE9_SUBEXP_MAX];
  char matches[5][512];
  int fail;
  const TestItem *ti = tests;

  int showAll = 1;
  if (argc > 1 && strcmp(argv[1], "fail") == 0) showAll = 0;

  for (; ti->re; ti++) {
    const char *errmsg;
    re9_prog_t *prog = re9_compile(ti->re, RE9_FLAG_NONUTF8, &errmsg);
    if (!prog) {
      printf("[%s]: error %s\n", ti->re, errmsg);
      return 1;
    }
    for (c = 0; c < 5; c++) matches[c][0] = '\0';
    if (showAll) printf("%s : %s : ", ti->re, ti->str); fflush(stdout);
    memset(subs, 0, sizeof(subs));
    if (re9_execute(prog, RE9_FLAG_NONUTF8, ti->str, subs, RE9_SUBEXP_MAX)) {
      /* save matched strings */
      int mn;
      for (mn = 0; mn < re9_nsub(prog); mn++) {
        if (subs[mn].sp && subs[mn].ep && subs[mn].sp != subs[mn].ep) {
          int len = (int)(ptrdiff_t)(subs[mn].ep-subs[mn].sp);
          memcpy(matches[mn], subs[mn].sp, len);
          matches[mn][len] = '\0';
        }
      }
    }
    /* check */
    fail = 0;
    for (c = 0; !fail && c < 5; c++) {
      if (ti->matches[c]) {
        if (strcmp(ti->matches[c], matches[c])) fail = 1;
      }
    }
    if (fail) {
      printf("---------------\n");
      if (!showAll) printf("%s : %s : ", ti->re, ti->str);
      printf("FAIL!\n");
      for (c = 0; c < 5; c++) {
        printf(" %d", c);
        if (ti->matches[c] && strcmp(ti->matches[c], matches[c])) {
          printf("\n  must: '%s'\n", ti->matches[c]);
          printf("  got : '%s'\n", matches[c]);
          printf("  (%s)\n", strcmp(ti->matches[c], matches[c])?"BAD":"OK");
        } else {
          printf(": OK\n");
        }
      }
    } else {
      if (showAll) printf("OK\n");
    }
    re9_free(prog);
  }

  return 0;
}
