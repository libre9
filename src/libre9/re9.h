/*
 * The authors of this software are Rob Pike and Ken Thompson.
 * Copyright (c) 2002 by Lucent Technologies.
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose without fee is hereby granted, provided that this entire notice
 * is included in all copies of any software which is or includes a copy
 * or modification of this software and in all copies of the supporting
 * documentation for such software.
 * THIS SOFTWARE IS BEING PROVIDED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED
 * WARRANTY.  IN PARTICULAR, NEITHER THE AUTHORS NOR LUCENT TECHNOLOGIES MAKE ANY
 * REPRESENTATION OR WARRANTY OF ANY KIND CONCERNING THE MERCHANTABILITY
 * OF THIS SOFTWARE OR ITS FITNESS FOR ANY PARTICULAR PURPOSE.
 *
 * heavily modified by Ketmar // Invisible Vector
 */
#ifndef _REGEXP9_H_
#define _REGEXP9_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>

/*
 * A regular expression specifies a set of strings of characters. A
 * member of this set of strings is said to be matched by the regular
 * expression. In the following specification for regular expressions the
 * word `character' means any character (rune) but newline.
 *
 * The syntax for a regular expression e0 is
 *   e3:  literal | charclass | '.' | '^' | '$' | '(?:' e0 ')' | '(' e0 ')'
 *
 *   e2:  e3
 *     |  e2 REP
 *
 *   REP: '*' | '+' | '?'
 *
 *   e1:  e2
 *     |  e1 e2
 *
 *   e0:  e1
 *     |  e0 '|' e1
 *
 *
 * A literal is any non-metacharacter, or a metacharacter (one of .*+?[]()|\^$).
 *
 * A charclass is a nonempty string s bracketed [s] (or [^s]); it matches any
 * character in (or not in) s. A negated character class never matches newline.
 * A substring a-b, with a and b in ascending order, stands for the inclusive
 * range of characters between a and b. In s, the metacharacters '-', ']', an
 * initial '^' must be preceded by a '\'; other metacharacters have no special
 * meaning and may appear unescaped.
 *
 * A '^' matches the beginning of a line; '$' matches the end of the line.
 * A '.' matches any character.
 * A '\z' matches character with code 0 ('\0').
 * A '\d' matches digits ([0-9]).
 * A '\s' matches space ([\f\t\r\n\v ]).
 * A '\w' matches 'word character' ([0-9A-Za-z_]).
 * A '\b' matches 'word boundary'.
 * One can use 'negative metacharacters' too ('\Z', '\D', etc.).
 *
 * The REP operators match zero or more (*), one or more (+), zero or one (?),
 * instances respectively of the preceding regular expression e2.
 *
 * An alternative regular expression, e0|e1, matches either a match to e0 or a match to e1.
 *
 * A match to any part of a regular expression extends as far as possible without preventing
 * a match to the remainder of the regular expression.
 *
 * Remember that POSIX classes ([:alpha:], etc) covers only ASCII characters!
 */


/* some cool defines:
 *
 * RE9_UNICODE_CASE
 *  define this to allow RE9_FLAG_CASEINSENS work with non-ascii characters
 *
 * RE9_DISABLE_NONGREEDY
 *  define this to disable non-greedy closures (the engine will be somewhat faster)
 *
 * RE9_DISABLE_POSIX_CLASSES
 *  disable parsing of posix classes like [:space:], [:digit:], etc.
 *
 * RE9_DISABLE_LEARNING
 *  disable regexp 'learning' in compiler
 */

#ifdef REGEXP9_DEBUG_MEMSIZE
extern int re9_memused;
#endif


/* maximum is 127, vm opcode limits this */
enum { RE9_SUBEXP_MAX = 16 };


/* subexpression matches */
typedef struct {
  const char *sp;
  const char *ep;
} re9_sub_t;


/* program definition */
typedef struct re9_prog_s re9_prog_t;


enum {
  RE9_FLAG_NONE = 0,
  RE9_FLAG_NONUTF8 = 0x01, /* for both compile and execute */
  RE9_FLAG_ANYDOT  = 0x02, /* '.' matches newline too; for both compile and execute */
  /* only for compiler */
  RE9_FLAG_LITERAL = 0x10,
  RE9_FLAG_NONGREEDY = 0x20, /* invert default repetition mode */
  RE9_FLAG_CASEINSENS = 0x40, /* only for ASCII */
  /* only for interpreter */
  RE9_FLAG_MT0_RANGE = 0x100, /* use match[0].sp and match[0].ep as string start and end */
  /* */
  RE9_FLAG_DUMPPRG = 0x1000
};


/*
 * re9_compile() compiles a regular expression and returns a pointer to the generated description.
 * re9_compile() returns 0 for an illegal expression or other failure.
 * Compiler is thread-safe.
 * errmsg can be NULL; if *errmsg is not NULL, it SHOULD NOT be free()d or modified!
 */
extern re9_prog_t *re9_compile (const char *s, int flags, const char **errmsg);

extern re9_prog_t *re9_compile_ex (const char *s, const char *eol, int flags, const char **errmsg);

/* return number of captures in this regexp; 0th capture is always full match */
extern int re9_nsub (const re9_prog_t *p);

/*
 * free compiled regular expression
 */
extern void re9_free (re9_prog_t *p);

/* re9_execute() matches a null-terminated string against the compiled regular expression in prog.
 * If it matches, regexec returns 1 and fills in the array match with character pointers to the
 * substrings of string that correspond to the parenthesized subexpressions of exp: match[i].sp
 * points to the beginning and match[i].ep points just beyond the end of the ith substring.
 * (Subexpression i begins at the ith left parenthesis, counting from 1.) Pointers in match[0]
 * pick out the substring that corresponds to the whole regular expression.
 * If match[0].sp is nonzero on entry, regexec starts matching at that point within string.
 * If match[0].ep is nonzero on entry, the last character matched is the one preceding that point.
 * Unused elements of match are filled with zeros. Matches involving and are extended as far as
 * possible. The number of array elements in match is given by msize.
 * mp can be NULL and ms should be 0 in this case.
 * re9_execute() returns 0 if string is not matched.
 * Executor is thread-safe, one program can be used in multiple threads simultaneously.
 */
/* progp: program to run
 * bol: string to run machine on
 * mp: subexpression elements (can be NULL)
 * ms: number of elements at mp (should be 0 if mp is NULL)
 */
extern int re9_execute (const re9_prog_t *progp, int flags, const char *bol, re9_sub_t *mp, int ms);

/* executor can eat up to maxmem memory */
extern int re9_execute_ex (const re9_prog_t *progp, int flags, const char *bol, re9_sub_t *mp, int ms, size_t maxmem);


/* 'prepared for execution' program */
typedef struct re9_prog_prepared_s re9_prog_prepared_t;


/* 'prepare' program -- allocate some memory, etc.
 * this can be used to avoid excessive malloc()s.
 * return NULL on error.
 */
extern re9_prog_prepared_t *re9_prepare (const re9_prog_t *progp);

/* executor can eat up to maxmem memory */
extern re9_prog_prepared_t *re9_prepare_ex (const re9_prog_t *progp, size_t maxmem);

/* execute 'prepared' program */
extern int re9_prepared_execute (re9_prog_prepared_t *pp, int flags, const char *bol, re9_sub_t *mp, int ms);

/* free 'prepared' program */
extern void re9_prepared_free (re9_prog_prepared_t *pp);


/* re9_sub() places in dp a substitution instance of sp in the context of the last regexec
 * performed using match. Each instance of `\n`, where n is a digit, is replaced by the string
 * delimited by match[n].sp and match[n].ep. Each instance of `\0` is replaced by the string
 * delimited by match[0].sp and match[0].ep. The substitution will always be null terminated
 * and trimmed to fit into dlen bytes.
 * Function will return number of bytes needed to successfully insert everything (including trailing 0).
 * Use '\{num}' to insert 10th and bigger match.
 * Use '\z' to insert character with code 0.
 */
/* sp: source string
 * dp: destination string
 * dlen: destination string size
 * mp: subexpression elements
 * ms: number of elements at mp
 */
extern int re9_sub (char *dp, size_t dlen, const char *sp, const re9_sub_t *mp, int ms);


/* re9_subst() places in dp a substitution instance of sp in the context of the last regexec
 * performed using match. Each instance of "$n", where n is a digit, is replaced by the string
 * delimited by match[n].sp and match[n].ep. Each instance of "$0" is replaced by the string
 * delimited by match[0].sp and match[0].ep. The substitution will always be null terminated
 * and trimmed to fit into dlen bytes.
 * Function will return number of bytes needed to successfully insert everything (including trailing 0).
 * Use "${num}" to insert 10th and bigger match.
 * Use `\z` to insert character with code 0.
 * Use `\xhh' to insert hex-encoded char.
 * Understands `\r`, `\n`, `\t`, `\e` as special codes.
 * Signle `\` MUST be escaped with another `\`!
 */
/* sp: source string
 * dp: destination string
 * dlen: destination string size
 * mp: subexpression elements
 * ms: number of elements at mp
 */
extern int re9_subst (char *dp, size_t dlen, const char *sp, const re9_sub_t *mp, int ms);


#ifdef __cplusplus
}
#endif
#endif
