#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>


static char *trim (char *str) {
  char *s;
  if (str == NULL) return NULL;
  for (s = str; *s && isspace(*s); ++s) ;
  if (s > str) memmove(str, s, strlen(s)+1);
  for (s = str+strlen(str)-1; s >= str && isspace(*s); --s) ;
  s[1] = 0;
  return str;
}


static int hex2num (char *s) {
  char *end;
  int res;
  trim(s);
  if (!s[0]) return -1;
  res = strtol(s, &end, 16);
  if (end[0] || res < 0) return -1;
  return res;
}


typedef struct {
  int code;
  const char *name;
  const char *class;
  int upper;
  int lower;
} UCInfo;


static char *stx = NULL;

static char *tok (void) {
  char *s = stx;
  for (; *stx && *stx != ';'; ++stx) ;
  if (*stx) *stx++ = 0;
  return s;
}


// <0: eof; 0: bad record; >0: good record
static int read_record (FILE *fi, UCInfo *ui) {
  static char str[8192];
  char *tk, *u, *l;
  int f;
  if (fgets(str, sizeof(str)-1, fi) == NULL) return -1;
  stx = trim(str);
  if ((tk = tok()) == NULL) return 0;
  if ((ui->code = hex2num(tk)) < 0) return 0;
  if (ui->code > 65535) return -1;
  if ((ui->name = trim(tok())) == NULL) return 0;
  if ((ui->class = trim(tok())) == NULL) return 0;
  // skip unused fields
  for (f = 9; f > 0; --f) if (tok() == NULL) { printf("%d\n", f); return 0; }
  if ((u = trim(tok())) == NULL) return 0;
  if ((l = trim(tok())) == NULL) return 0;
  if (!u[0]) ui->upper = ui->code; else ui->upper = hex2num(u);
  if (!l[0]) ui->lower = ui->code; else ui->lower = hex2num(l);
  if (ui->upper < 0 || ui->lower < 0) return 0;
  if (ui->upper > 65535 || ui->lower > 65535) abort();
  return 1;
}


typedef struct {
  int code, l, u;
} mm;

static mm map[65535];


static int mm_cmp (const void *p0, const void *p1) {
  const mm *i0 = (const mm *)p0;
  const mm *i1 = (const mm *)p1;
  return (i0->code < i1->code ? -1 : (i0->code > i1->code ? 1 : 0));
}


int main (int argc, char *argv[]) {
  UCInfo ui;
  int rc, f, totalmap = 0;
  FILE *fi, *fo;
  if (argc != 3) { fprintf(stderr, "usage: %s unitable.c unitable.txt\n", argv[0]); exit(1); }
  if ((fi = fopen(argv[2], "r")) == NULL) { fprintf(stderr, "FATAL: can't open input file: '%s'\n", argv[2]); exit(1); }
  for (;;) {
    rc = read_record(fi, &ui);
    if (rc < 0) break;
    if (rc == 0) continue;
    if (ui.code < 128) continue;
    if (strcmp(ui.class, "Lu") != 0 && strcmp(ui.class, "Ll") != 0) continue;
    if (ui.upper == ui.code && ui.lower == ui.code) continue;
    for (f = 0; f < totalmap; ++f) {
      if (map[f].code == ui.code) { fprintf(stderr, "FATAL: duplicate entries in the map!\n"); exit(1); }
      if (map[f].code > ui.code) { fprintf(stderr, "FATAL: invalid entry order in the map!\n"); exit(1); }
    }
    if (totalmap > 65535) { fprintf(stderr, "FATAL: too many entries in the map!\n"); exit(1); }
    map[totalmap].code = ui.code;
    map[totalmap].l = ui.lower;
    map[totalmap].u = ui.upper;
    ++totalmap;
  }
  fclose(fi);
  qsort(map, totalmap, sizeof(map[0]), mm_cmp);
  fo = fopen(argv[1], "w");
  if (fo == NULL) { fprintf(stderr, "FATAL: can't create output file: '%s'\n", argv[1]); exit(1); }
  fprintf(fo, "static const struct casemap unicode_case_mapping[%d] = {\n", totalmap);
  for (f = 0; f < totalmap; ++f) fprintf(fo, "{0x%04x,0x%04x,0x%04x},\n", map[f].code, map[f].l, map[f].u);
  fprintf(fo, "%s\n", "};");
  fclose(fo);
  return 0;
}
