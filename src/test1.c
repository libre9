#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "libre9/re9.h"


int main (int argc, char *argv[]) {
  re9_sub_t rs[10];
  re9_prog_t *p;
  re9_prog_prepared_t *prp;
  const char *errmsg, *restr;
  int flg = RE9_FLAG_NONUTF8;
  /* */
  if (argc > 1 && strcmp(argv[1], "-d") == 0) {
    flg |= RE9_FLAG_DUMPPRG;
    argv[1] = argv[0];
    ++argv;
    --argc;
  }
  /* */
  if (argc < 3) { fprintf(stderr, "usage: %s regexp string...\n", argv[0]); exit(1); }
  /* */
  restr = argv[1];
  if (restr[0] == '/' && strchr(restr+1, '/')) {
    for (++restr; *restr != '/'; ++restr) {
      switch (*restr) {
        case 'i': flg |= RE9_FLAG_CASEINSENS; break;
        case 'u': flg &= ~RE9_FLAG_NONUTF8; break;
        default:
          fprintf(stderr, "invalid regexp option: '%c'\n", *restr);
          exit(1);
      }
    }
    ++restr;
  }
  p = re9_compile(restr, flg, &errmsg);
  if (p == NULL) { fprintf(stderr, "FATAL: invalid regexp '%s': %s\n", argv[1], errmsg); exit(2); }
  if ((prp = re9_prepare(p)) == NULL) { fprintf(stderr, "FATAL: can't prepare regexp!\n"); exit(2); }
  /* */
  for (int f = 2; f < argc; ++f) {
    int res;
    if (f > 2) printf("===============================\n");
    printf("(%d) %s VIA %s\n", re9_nsub(p), argv[f], argv[1]);
    if ((res = re9_prepared_execute(prp, flg, argv[f], rs, 10)) > 0) {
      for (int c = 0; c < 10; ++c) {
        if (rs[c].sp != NULL && rs[c].ep != NULL) {
          printf(" %d:(%d,%d) <", c, rs[c].sp-argv[f], rs[c].ep-argv[f]);
          fwrite(rs[c].sp, rs[c].ep-rs[c].sp, 1, stdout);
          fputc('>', stdout);
        }
      }
    } else if (res < 0) {
      printf("OUT OF MEMORY!");
    }
    printf("\n");
  }
  /* */
  re9_prepared_free(prp);
  re9_free(p);
  /* */
  return 0;
}
