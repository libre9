#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libre9/re9.h"


int main (int argc, char *argv[]) {
  re9_prog_t *p;
  const char *errmsg;
  if (argc < 4) {
    fprintf(stderr, "usage: %s regexp substr string string...\n", argv[0]);
    return 1;
  }
  p = re9_compile(argv[1], RE9_FLAG_NONUTF8, &errmsg);
  if (p == NULL) { fprintf(stderr, "FATAL: invalid regexp '%s': %s\n", argv[1], errmsg); return 1; }
  for (int f = 3; f < argc; ++f) {
    re9_sub_t rs[10];
    if (f > 3) printf("===============================\n");
    if (re9_execute(p, RE9_FLAG_NONUTF8, argv[f], rs, 10)) {
      static char dst[8192];
      int sz = re9_sub(dst, sizeof(dst), argv[2], rs, re9_nsub(p));
      printf(" nsub=%d; sub %s -> <%s> (%d)\n", re9_nsub(p), argv[2], dst, sz);
    }
  }
  re9_free(p);
  return 0;
}
