#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libre9/re9.h"


static void TEST (const char *re, const char *s, int start, int size) {
  re9_sub_t rs[10];
  re9_prog_t *p;
  const char *errmsg;
  p = re9_compile(re, RE9_FLAG_NONUTF8, &errmsg);
  if (p == NULL) { fprintf(stderr, "FATAL: invalid regexp '%s': %s\n", re, errmsg); return; }
  if (re9_execute(p, RE9_FLAG_NONUTF8, s, rs, 10)) {
    if (rs[0].sp-s != start || rs[0].ep-s != start+size) {
      printf("ERROR: re=<%s>; s=<%s> %u %u (%d %d)\n", re, s, rs[0].sp-s, rs[0].ep-s, start, start+size);
    } else {
      printf("OK: re=<%s>; s=<%s>\n", re, s);
    }
  } else {
    if (start >= 0) {
      printf("ERROR: re=<%s>; s=<%s> %u %u (%d %d)\n", re, s, rs[0].sp-s, rs[0].ep-s, start, start+size);
    } else {
      printf("OK: re=<%s>; s=<%s>\n", re, s);
    }
  }
  re9_free(p);
}


int main() {
  TEST("", "a", 0, 0);
  TEST("a", "a", 0, 1);
  TEST("aa", "a", -1, 0);
  TEST("aa", "aaaaa", 0, 2);
  TEST("baa", "aaaaa", -1, 0);
  TEST("aab", "aa", -1, 0);
  TEST("aab", "caaab", 2, 3);
  TEST("ab*a", "caaabbba", 1, 2);
  TEST("acb*a", "aaacbbba", 2, 6);
  TEST("ab+a", "aaabbba", 2, 5);
  TEST("b[0-9]", "aabbab123bba", 5, 2);
  TEST("b[0-9]?", "ab", 1, 1);
  TEST("b[0-9]?", "ab5", 1, 2);
  TEST("b[0-9]?c", "ab5c", 1, 3);
  TEST("b[0-9]?c", "abc", 1, 2);
  TEST("a[0-9]+", "a", -1, 0);
  TEST("a[0-9]+", "a1", 0, 2);
  TEST("a[0-9]+", "a123", 0, 4);
  TEST("b[0-9]+", "aabbab123bba", 5, 4);
  TEST("a[0-9]*", "ab", 0, 1);
  TEST("a[0-9]*", "a123b", 0, 4);
  TEST("a[0-9]*?", "ab", 0, 1);
  TEST("a[0-9]*?", "a123", 0, 1);
  TEST("a[0-9]*?c", "a123c", 0, 5);
  TEST("a[0-9]*?[0-9]", "a123", 0, 2);

  TEST("a.*c", "abc", 0, 3);

  TEST("\\.", "a", -1, 0);
  TEST("\\.", ".", 0, 1);
  TEST("\\^", "a", -1, 0);
  TEST("\\^", "a^", 1, 1);
  TEST("\\$", "a", -1, 0);
  TEST("\\$", "a$a", 1, 1);

  return 0;
}
