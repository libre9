#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "libre9/re9.h"


struct x {
  const char *re;
  const char *s;
  re9_prog_t *p;
};


static struct x t[] = {
  { "^[^!@]+$", "/bin/upas/aliasmail '\\0'", 0 },
  { "^local!(.*)$", "/mail/box/\\1/mbox", 0 },
  { "^plan9!(.*)$", "\\1", 0 },
  { "^helix!(.*)$", "\\1", 0 },
  { "^([^!]+)@([^!@]+)$", "\\2!\\1", 0 },
  { "^(uk\\.[^!]*)(!.*)$", "/bin/upas/uk2uk '\\1' '\\2'", 0 },
  { "^[^!]*\\.[^!]*!.*$", "inet!\\0", 0 },
  { "^\xE2\x98\xBA$", "smiley", 0 },
  { "^(coma|research|pipe|pyxis|inet|hunny|gauss)!(.*)$", "/mail/lib/qmail '\\s' 'net!\\1' '\\2'", 0 },
  { "^.*$", "/mail/lib/qmail '\\s' 'net!research' '\\{0}'", 0 },
  { 0, 0, 0 },
};


int main (int argc, char *argv[]) {
  re9_sub_t rs[10];
  static char dst[8192];
  struct x *tp;
  /* */
  for (tp = t; tp->re; ++tp) tp->p = re9_compile(tp->re, RE9_FLAG_NONUTF8, NULL);
  /* */
  for (int f = 1; f < argc; ++f) {
    if (f > 1) printf("===============================\n");
    for (tp = t; tp->re; ++tp) {
      printf("<%s> VIA <%s>", argv[f], tp->re);
      if (re9_execute(tp->p, RE9_FLAG_NONUTF8, argv[f], rs, 10)) {
        int sz = re9_sub(dst, sizeof(dst), tp->s, rs, 10);
        printf(" sub %s -> <%s> (%d)", tp->s, dst, sz);
      }
      printf("\n");
    }
  }
  /* */
  for (tp = t; tp->re; ++tp) re9_free(tp->p);
  /* */
  return 0;
}
